# Rackup Template

A project meant to speed up the creation of Grape based microservices.

Opinionated, of course.


## Usage

The following will create `fancy-new-project` in the current directory:

```bash
git clone https://gitlab.com/dot_lexg/rackup-template
rackup-template/exe/clone fancy-new-project
cd fancy-new-project
rerun rackup
```

You can choose an alternate parent directory. The following will create `fancy-new-project` within `~/Projects`:

```bash
git clone https://gitlab.com/dot_lexg/rackup-template
rackup-template/exe/clone -C ~/Projects fancy-new-project
cd ~/Projects/fancy-new-project
rerun rackup
```


## Next Steps

- Refer to [the grape README](https://github.com/ruby-grape/grape/blob/master/README.md) for usage of grape.
