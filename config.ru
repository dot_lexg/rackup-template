# frozen_string_literal: true

require 'bundler/setup'
Bundler.require

loader = Zeitwerk::Loader.new
loader.push_dir(File.expand_path('lib', __dir__))
loader.inflector.inflect 'api' => 'API'
loader.setup

unless ENV['RACK_ENV'] == 'development'
	loader.eager_load
	Template::API.compile!
end

run Template::API
