# frozen_string_literal: true

class Template::API < Grape::API
	format :json

	get do
		{
			application: 'template',
			version: Template::VERSION,
			environment: ENV['RACK_ENV'],
			rack_env: env
		}
	end
end
